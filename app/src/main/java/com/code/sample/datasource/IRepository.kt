package com.code.sample.datasource

import androidx.lifecycle.LiveData
import com.code.sample.model.Company
import com.code.sample.model.Product

interface IRepository {
    fun getProductList(): LiveData<List<Product>>
    fun getProduct(id: String): LiveData<Product>
//    fun getCompanyList(): LiveData<List<Company>>
    fun getCompany(id: String): LiveData<Company>
}