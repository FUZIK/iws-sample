package com.code.sample.datasource

import com.code.sample.activity.HostActivity

object Repository {
    fun getDefault(): IRepository {
        return JsonRepoImpl.getInstance(HostActivity.getInstance().applicationContext)
    }
}