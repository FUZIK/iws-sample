package com.code.sample.datasource

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.code.sample.model.Company
import com.code.sample.model.Product
import com.code.sample.util.ResUtil
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

const val JSON_REPO_PATH = "repository/json/product_and_company_array.json"

class JsonRepoImpl(private val context: Context) : IRepository {

    private var productJsonArray = JSONArray("[]")
    private var companyJsonArray = JSONArray("[]")

    init {
        loadFromRepo()
    }

    private fun loadFromRepo() {
        val jsonRepo = getRepoJson()
        productJsonArray = jsonRepo.getJSONArray("products")
        companyJsonArray = jsonRepo.getJSONArray("companies")
    }

    private fun getRepoJson(): JSONObject {
        var stringJson = JSONObject("{}")
        try {
            val inputStream = context.assets.open(JSON_REPO_PATH)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.use { it.read(buffer) }
            stringJson = JSONObject(String(buffer))
        } catch (ioException: IOException) {
            ioException.printStackTrace()
        }
        return stringJson
    }

    private fun getProductJson(jsonObject: JSONObject) =
        Product(
            jsonObject.getString("id"),
            jsonObject.getString("name"),
            jsonObject.getString("description"),
            jsonObject.getString("company_id"),
            ResUtil.uriFromRes(jsonObject.getString("iconRes"), context)
        )

    private fun getProductListJson(jsonArray: JSONArray): List<Product> {
        val resultProductList = mutableListOf<Product>()
        for (i in 0 until jsonArray.length()) {
            resultProductList.add(getProductJson(jsonArray.getJSONObject(i)))
        }
        return resultProductList
    }

    private fun getCompanyJson(jsonObject: JSONObject) =
        Company(
            jsonObject.getString("id"),
            jsonObject.getString("name"),
            jsonObject.getString("webSite"),
            ResUtil.uriFromRes(jsonObject.getString("iconRes"), context)
        )

    override fun getProductList(): LiveData<List<Product>> {
        return MutableLiveData<List<Product>>().apply {
            value = getProductListJson(productJsonArray)
        }
    }

    override fun getProduct(id: String): LiveData<Product> {
        var productResult = Product.EMPTY
        for (i in 0 until productJsonArray.length()) {
            val jsonObj = productJsonArray.getJSONObject(i)
            if (jsonObj.getString("id") == id) {
                productResult = getProductJson(jsonObj)
            }
        }
        return MutableLiveData<Product>().apply {
            value = productResult
        }
    }

    override fun getCompany(id: String): LiveData<Company> {
        var companyResult = Company.EMPTY
        for (i in 0 until companyJsonArray.length()) {
            val jsonObj = companyJsonArray.getJSONObject(i)
            if (jsonObj.getString("id") == id) {
                companyResult = getCompanyJson(jsonObj)
            }
        }
        return MutableLiveData<Company>().apply {
            value = companyResult
        }
    }

    companion object {
        private var instance: JsonRepoImpl? = null
        fun getInstance(context: Context): JsonRepoImpl {
            synchronized(this) {
                return instance ?: JsonRepoImpl(context)
            }
        }
    }
}
