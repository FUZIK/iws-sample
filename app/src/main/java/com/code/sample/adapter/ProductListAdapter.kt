package com.code.sample.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.code.sample.R
import com.code.sample.holder.OnProductClickListener
import com.code.sample.holder.ProductHolder
import com.code.sample.model.Product
import java.util.zip.Inflater

class ProductListAdapter(private val onClickListener: OnProductClickListener) : RecyclerView.Adapter<ProductHolder>() {
    // TODO: move to main constructor?
    var productList = emptyList<Product>()
    set(value) {
        notifyItemRangeChanged(0, field.size)
        field = value
    }

    fun get(id: Int) = productList[id]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.product_element, parent, false)
        return ProductHolder(view, onClickListener)
    }

    override fun getItemCount(): Int =
        productList.size

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        holder.bind(productList[position])
    }
}