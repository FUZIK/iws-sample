package com.code.sample.util

import android.content.ContentResolver
import android.content.Context
import android.content.res.Resources
import android.net.Uri
import android.util.Log


object ResUtil {
    fun uriFromRes(resId: String, context: Context): Uri {
        val resNameSplit = resId.split('.', limit = 3)
        val packageName = context.packageName
        val resType = resNameSplit[1]
        val resName = resNameSplit[2]
        Log.wtf("WTF", resNameSplit.joinToString("','"))
        return uriFromRes(context.resources.getIdentifier(resName, resType, packageName), context)
    }

    fun uriFromRes(resId: Int, context: Context): Uri {
        val resources = context.resources
        return Uri.Builder()
            .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
            .authority(resources.getResourcePackageName(resId))
            .appendPath(resources.getResourceTypeName(resId))
            .appendPath(resources.getResourceEntryName(resId))
            .build()
    }
}