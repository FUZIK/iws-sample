package com.code.sample.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.code.sample.R
import com.code.sample.activity.HostActivity
import com.code.sample.datasource.Repository
import com.code.sample.model.Product
import com.code.sample.viewmodel.ProductModel

class ProductFragment : Fragment() {
    private val productModel: ProductModel by viewModels()
    private lateinit var productIcon: ImageView
    private lateinit var productName: TextView
    private lateinit var productId: TextView
    private lateinit var productDescription: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.product_fragment, container, false)
        productIcon = view.findViewById(R.id.product_icon)
        productName = view.findViewById(R.id.product_name)
        productId = view.findViewById(R.id.product_id)
        productDescription = view.findViewById(R.id.product_description)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupToolbar()
        val args = arguments
        if (savedInstanceState == null && args != null) {
            val productId = args.getString(PRODUCT_ID_KEY)
            if (productId != null) {
                Repository.getDefault().getProduct(productId).observe(this) {
                    productModel.productLive.value = it
                }
            }
        }
        productModel.productLive.observe(this) {
            bind(it)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            R.id.menu_about_company -> {
                val companyFragment = CompanyFragment()
                val bundle = Bundle()
                productModel.productLive.value?.let {
                    bundle.putString(CompanyFragment.COMPANY_ID_KEY, it.companyId)
                    companyFragment.arguments = bundle
                    HostActivity.getInstance().nextFragment(companyFragment)
                }
                true
            }
            R.id.menu_up -> {
                activity!!.onBackPressed()
                true
            }
            else -> false
        }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.product_fragment_menu, menu)
    }

    override fun onResume() {
        super.onResume()
        setupToolbar()
    }

    fun setupToolbar() {
        setHasOptionsMenu(true)
    }

    fun bind(product: Product) {
        productIcon.setImageURI(product.productIconUri)
        productName.text = product.name
        productId.text = product.id
        productDescription.text = product.description
    }


    companion object {
        const val PRODUCT_ID_KEY = "product_id_key"
    }
}
