package com.code.sample.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.code.sample.R
import com.code.sample.activity.HostActivity
import com.code.sample.adapter.ProductListAdapter
import com.code.sample.datasource.Repository
import com.code.sample.holder.OnProductClickListener
import com.code.sample.holder.ProductHolder
import com.code.sample.viewmodel.ProductListModel

class ProductListFragment : Fragment(), OnProductClickListener {
    private val productListModel: ProductListModel by viewModels()
    private lateinit var productRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.product_list_fragment, container, false)
        productRecyclerView = view.findViewById(R.id.product_list)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState == null) {
            productListModel.productListAdapter = ProductListAdapter(this).apply {
                productListModel.productLiveList.observeForever {
                    it?.let {
                        productList = it
                        Log.wtf("LIST", "update adapter ${it.size}")
                    }
                }
            }
            Repository.getDefault().getProductList().observe(this) {
                productListModel.productLiveList.value = it
                Log.wtf("LIST", "updated! ${it.size}")
            }
        }
        productRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = productListModel.productListAdapter
        }
    }

    override fun onProductClick(position: Int) {
        val product = productListModel.productListAdapter.get(position)
        val productFragment = ProductFragment()
        val bundle = Bundle()
        bundle.putString(ProductFragment.PRODUCT_ID_KEY, product.id)
        productFragment.arguments = bundle
        HostActivity.getInstance().nextFragment(productFragment)
    }
}