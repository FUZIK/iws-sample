package com.code.sample.fragment

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.code.sample.R
import com.code.sample.datasource.Repository
import com.code.sample.model.Company
import com.code.sample.viewmodel.CompanyModel

class CompanyFragment : Fragment() {
    private val companyModel: CompanyModel by viewModels()
    private lateinit var companyIcon: ImageView
    private lateinit var companyName: TextView
    private lateinit var companyWebSite: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.company_fragment, container, false)
        companyIcon = view.findViewById(R.id.company_icon)
        companyName = view.findViewById(R.id.company_name)
        companyWebSite = view.findViewById(R.id.company_web_site)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupToolbar()
        val args = arguments
        if (savedInstanceState == null && args != null && args.containsKey(COMPANY_ID_KEY)) {
            val companyId = args.getString(COMPANY_ID_KEY)
            if (companyId != null) {
                Repository.getDefault().getCompany(companyId).observe(this) {
                    companyModel.companyLive.value = it
                }
            }
        }
        companyModel.companyLive.observe(this) { company ->
            companyWebSite.setOnClickListener {
                company?.let {
                    AlertDialog.Builder(context)
                        .setTitle("Переход по ссылке\n ${company.webSite}")
                        .setPositiveButton("угу") { alertWindow, _ ->
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(company.webSite)))
                            alertWindow.cancel()
                        }
                        .setNegativeButton("не сейчас") { alertWindow, _ ->
                            alertWindow.cancel()
                        }.show()
                }
            }
            bind(company)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            R.id.menu_up -> {
                activity!!.onBackPressed()
                true
            }
            else -> false
        }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.company_fragment_menu, menu)
    }

    override fun onResume() {
        super.onResume()
        setupToolbar()
    }

    fun setupToolbar() {
        setHasOptionsMenu(true)
    }

    fun bind(company: Company) {
        companyIcon.setImageURI((company.companyIconUri))
        companyName.text = company.name
        companyWebSite.text = Html.fromHtml("<a href='${company.webSite}'>${company.webSite}</a>")
    }


    companion object {
        const val COMPANY_ID_KEY = "company_id_key"
    }
}