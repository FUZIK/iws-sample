package com.code.sample.model

import android.net.Uri

data class Product(
    val id: String,
    val name: String,
    val description: String,
    val companyId: String,
    val productIconUri: Uri
) {
    companion object {
        val EMPTY = Product("-", "-", "-", "-", Uri.EMPTY)
    }
}