package com.code.sample.model

import android.net.Uri

data class Company(
    val id: String,
    val name: String,
    val webSite: String,
    val companyIconUri: Uri
) {
    companion object {
        val EMPTY = Company("-", "-", "-", Uri.EMPTY)
    }
}