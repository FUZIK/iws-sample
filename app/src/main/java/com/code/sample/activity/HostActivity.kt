package com.code.sample.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.code.sample.R
import com.code.sample.fragment.ProductListFragment

class HostActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.host_activity)
        instance = this
        setSupportActionBar(findViewById(R.id.toolbar))
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.host_container, ProductListFragment())
                .commit()
        }
    }

    fun nextFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.host_container, fragment)
            .addToBackStack("ROOT_STACK")
            .commit()
    }

    override fun onBackPressed() {
        supportFragmentManager.popBackStackImmediate()
    }

    companion object {
        private lateinit var instance: HostActivity
        fun getInstance(): HostActivity {
            synchronized(this) {
                return instance
            }
        }
    }
}
