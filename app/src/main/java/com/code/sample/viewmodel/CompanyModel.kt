package com.code.sample.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.code.sample.model.Company

class CompanyModel : ViewModel() {
    val companyLive = MutableLiveData<Company>()
}