package com.code.sample.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.code.sample.adapter.ProductListAdapter
import com.code.sample.model.Product

class ProductListModel : ViewModel() {
    val productLiveList = MutableLiveData<List<Product>>()
    //TODO: remove forever observer after model death
    lateinit var productListAdapter: ProductListAdapter
}