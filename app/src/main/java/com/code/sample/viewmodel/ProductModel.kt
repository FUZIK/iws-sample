package com.code.sample.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.code.sample.model.Product

class ProductModel : ViewModel() {
    val productLive = MutableLiveData<Product>()
}