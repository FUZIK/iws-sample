package com.code.sample.holder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.code.sample.R
import com.code.sample.model.Product

class ProductHolder(view: View,
                    private val onClickListener: OnProductClickListener) : RecyclerView.ViewHolder(view), View.OnClickListener {
    val idText: TextView = view.findViewById(R.id.product_id)
    val nameText: TextView = view.findViewById(R.id.product_name)

    init {
        view.setOnClickListener(this)
    }

    fun bind(product: Product) {
        idText.text = product.id
        nameText.text = product.name
    }

    override fun onClick(p0: View?) {
        onClickListener.onProductClick(adapterPosition)
    }
}

interface OnProductClickListener {
    fun onProductClick(position: Int)
}